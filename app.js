const express = require('express');
const path = require('path'); // Import the path module
const app = express();

const userRouter = require('./routes/userRoutes');
const viewRouter = require('./routes/viewRoutes');
const cookieParser = require('cookie-parser')
app.use(cookieParser())

app.use(express.json());
app.use('/api/v1/users', userRouter);
app.use('/',viewRouter);

// Serve static files from the 'views' directory
app.use(express.static(path.join(__dirname, 'views')));

module.exports = app; // Export the Express app
