const mongoose = require('mongoose');
const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });
const app = require('./app'); // Import the Express app from app.js

const DB = process.env.DATABASE.replace('Pbhutan', process.env.DATABASE_PASSWORD);

const local_DB=process.env.DATABASE_LOCAL

mongoose.connect(local_DB).then((con) => {
        // console.log(con.connections)
        console.log('DB connection successful');
    })
    .catch(error => console.log(error));

const port = 4001;

app.listen(port, () => {
    console.log(`App is running on port ${port}`);
});
